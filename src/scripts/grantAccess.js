import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import config from "../firebase";
import { access } from 'fs';
require("date-utils");


firebase.initializeApp(config);


//Grant Access 
var userError = document.getElementById("userError");
var outFirstName = document.getElementById("outFirstName");
var outLastName = document.getElementById("outLastName");
var outEmail = document.getElementById("outEmail");
var emailError = document.getElementById("emailError");
var firstNameError = document.getElementById("firstNameError");
var lastNameError = document.getElementById("lastNameError");

//When user clicks search, validate entries before searching for the user in the database
document.getElementById("search").addEventListener("click", evt => {
    evt.preventDefault();
    var email = document.getElementById("email").value;
    var firstName = document.getElementById("firstName").value;
    var lastName = document.getElementById("lastName").value;
    var UTCdate = getUTCDateTime(document.getElementById("date").value);

    //Check Email
    if (validateEmail(email) && email !== undefined) {
        var verifiedEmail = email;
        sessionStorage.setItem("verifiedEmail", verifiedEmail);
        console.log(sessionStorage.verifiedEmail);
    }
    else {
        emailError.innerHTML = "Enter an valid email into the email field.";
    }
    //Check First Name
    if (validateTextOnly(firstName) && firstName !== undefined) {
        var verifiedFirstName = firstName;
        sessionStorage.setItem("verifiedFirstName", verifiedFirstName);
    }
    else {
        firstNameError.innerHTML = "First name can only contain letters.";
    }
    //Check Last Name
    if (validateTextOnly(lastName) && lastName !== undefined) {
        var verifiedLastName = lastName;
        sessionStorage.setItem("verifiedLastName", verifiedLastName);
    }
    else {
        lastNameError.innerHTML = "Last name can only contain letters.";
    }
    //Check Date
    if (getUTCDateTime() < UTCdate) {
        var verifiedDate = UTCdate;
        sessionStorage.setItem("endAccessDate", verifiedDate);
    }
    else {
        document.getElementById("dateError").innerHTML = "The date must be in the future";
    }

    checkUserExists();

})

//If Session variables exist for the fields, show the admin the user details
function checkUserExists() {
    if (!(sessionStorage.endAccessDate) || !(sessionStorage.verifiedEmail) || !(sessionStorage.verifiedFirstName) || !(sessionStorage.verifiedLastName)) {
        userError.innerHTML = "Please check the fields before continuing."
    }
    else {
        var verifiedEmail = sessionStorage.verifiedEmail;
        var verifiedDate = sessionStorage.endAccessDate;
        firebase.database().ref("user/").orderByChild("email").equalTo(verifiedEmail).once("value", function (snapshot) {
            if (snapshot.exists()) {
                snapshot.forEach(function (childSnapshot) {
                    outFirstName.innerHTML = childSnapshot.val().firstName;
                    outLastName.innerHTML = childSnapshot.val().lastName;
                    outEmail.innerHTML = childSnapshot.val().email;
                    outDate.innerHTML = verifiedDate;
                    var userKey = childSnapshot.key;
                    sessionStorage.userKey = userKey;
                    userError.innerHTML = "We found this user. Click below to grant access to the date chosen.";
                });
            }
            else if (snapshot.val() == null) {
                outFirstName.innerHTML = sessionStorage.verifiedFirstName;
                outLastName.innerHTML = sessionStorage.verifiedLastName;
                outEmail.innerHTML = verifiedEmail;
                outDate.innerHTML = verifiedDate;
                userError.innerHTML = "This user is not yet in our system. Please verify the details and click below to grant access and add them now.";
            }
        })
    }
}


//On click of Grant Free Access, check if session variables exist, if so then grant free access
document.getElementById("grant-free").addEventListener("click", evt => {
    evt.preventDefault();
    if (!(sessionStorage.endAccessDate) || !(sessionStorage.verifiedEmail) || !(sessionStorage.verifiedFirstName) || !(sessionStorage.verifiedLastName)) {
        userError.innerHTML = "Please check the user input fields before continuing."
    } else {
        if ((sessionStorage.userKey) && sessionStorage.userKey !== "user") {
            logActivity();
            updateUser();
            userError.innerHTML = "The user's access was successfully modified.";
        }
        else {
            logActivity();
            createUser();
        }
    }

})

//On click of cancel, clear the session data
document.getElementById("cancel").addEventListener("click", ()=>{
    sessionStorage.removeItem("userKey");
    sessionStorage.removeItem("verifiedFirstName");
    sessionStorage.removeItem("endAccessDate");
    sessionStorage.removeItem("verifiedEmail");
    sessionStorage.removeItem("verifiedLastName");
})

function logActivity(email) {
    var sUser = sessionStorage.sUser;
    var dateToday = getUTCDateTime();
    var email = sessionStorage.verifiedEmail;
    firebase.database().ref("user/" + sUser + "/Activity/" + dateToday).set({
        type: "Granted Access",
        user: email
    })
}

function updateUser() {
    var userKey = sessionStorage.userKey;
    var dateExtend = sessionStorage.endAccessDate;
    firebase.database().ref("user/" + userKey + "/payment/" + dateExtend).set({
        paymentMethod: "Granted by Admin",
    })
    firebase.database().ref('user/' + userKey).update({
        userType: "paid"
    })
    sessionStorage.removeItem("userKey");
    sessionStorage.removeItem("verifiedDate");
    sessionStorage.removeItem("verifiedEmail");
    
}

function createUser() {
    const dateToday = getUTCDateTime();
    const password = "password";
    const email = sessionStorage.verifiedEmail;
    const firstName = sessionStorage.verifiedFirstName;
    const lastName = sessionStorage.verifiedLastName;
    const UTCdate = sessionStorage.endAccessDate;
    firebase.auth().createUserWithEmailAndPassword(email, password).then(
        () => {
            //Save information for the user
            const sUser = firebase.auth().currentUser.uid;
            firebase.database().ref("user/" + sUser).set({
                userType: "paid",
                email: email,
                password: "password",
                firstName: firstName,
                lastName: lastName,
                dateCreated: dateToday,
                userType: "paid"
            })

            firebase.database().ref("user/" + sUser + "/payment/" + UTCdate).set({
                paymentMethod: "Granted by Admin",
            })
        }
    ).then(() => {
        firebase.auth().sendPasswordResetEmail(email);
    });
    sessionStorage.removeItem("verifiedFirstName");
    sessionStorage.removeItem("endAccessDate");
    sessionStorage.removeItem("verifiedEmail");
    sessionStorage.removeItem("verifiedLastName");
    userError.innerHTML = "The user was created and modified. <br> An email will be sent to reset their password.";

}

function getUTCDateTime(date) {
    if (date !== undefined) {
        var now = new Date(date);
    }
    else {
        var now = new Date();
    }
    return now.getUTCFullYear() + '-' + (now.getUTCMonth() + 1) + '-' + now.getUTCDate() + ' ' + now.getUTCHours() + ':' + now.getUTCMinutes() + ':' + now.getUTCSeconds()
}

//Regex for Email - Validate email is well formed
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validateTextOnly(name) {
    return /^[a-zA-Z]+$/.test(name);
}

//Password must be at least 6 letters long
function validatePassword(password) {
    var re = /^(?=.{6,})/;
    return re.test(password);
}