import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import config from "../firebase";
import { access } from 'fs';
import { start } from 'repl';
require("date-utils");


firebase.initializeApp(config);

if (sessionStorage.userType == "admin") {
    console.log("");
}
else if (sessionStorage.userType == "pending") {
    window.location.replace("../pages/payment.html?status=pending");
}
else if (sessionStorage.userType == "paid") {
    window.location.replace("../pages/dashboard.html");

}
else {
    window.location.replace("../index.html");
}

//Report -- access date
document.getElementById("report").addEventListener("click", evt => {
    evt.preventDefault();
    var startDate = getUTCDateTime(document.getElementById("startDate").value);
    var endDate = getUTCDateTime(document.getElementById("endDate").value);
    if (endDate>Date.now()){
        document.getElementById("reportError").innerHTML="The end date cannot be later than todays date.";
    }
    else if(endDate<startDate){
        document.getElementById("reportError").innerHTML = "The end date cannot be earlier than the start date";
    }
    else{

    var aStartDateSplit = startDate.split(' ');
    startDate = aStartDateSplit[0];
    var aEndDateSplit = endDate.split(' ');
    endDate = aEndDateSplit[0];

    firebase.database().ref("user/").once("value")
        .then(function (users) {
            var aUsers = [];
            users.forEach(function (user) {
                var oUser = user.val();
                if (oUser.report) {
                    var aUserReportKeys = Object.keys(oUser.report);
                    for (var j = 0; j < aUserReportKeys.length; j++) {
                        var dateSearched = oUser.report[aUserReportKeys[j]].dateSearched;
                        var aDateSearchedSplit = dateSearched.split(' ');
                        dateSearched = aDateSearchedSplit[0];
                        console.log("Start Date: " + startDate);
                        console.log("End Date: " + endDate);
                        console.log("Date Searched from DB: " + dateSearched);

                        if (dateSearched >= startDate && dateSearched <= endDate) {
                            var objUser = {};
                            objUser.firstName = oUser.firstName;
                            objUser.lastName = oUser.lastName;
                            objUser.email = oUser.email;
                            objUser.dateSearched = dateSearched;
                            objUser.userType = oUser.userType;
                            aUsers.push(objUser);
                        }
                    }
                }
            });
            createReportTable(aUsers);
        })
    }
})


function createReportTable(aUsers) {
    console.log(aUsers.length);
    var aReportTableHeaders = ["First Name", "Last Name", "Email ID", "Date Accessed", "User Type"];
    var aReportRowKeys = ["firstName", "lastName", "email", "dateSearched", "userType"];


    var table = document.createElement("table");


    var tr = table.insertRow(-1);

    for (var i = 0; i < aReportTableHeaders.length; i++) {
        var th = document.createElement("th");
        th.innerHTML = aReportTableHeaders[i];
        tr.appendChild(th);
    }

    for (var i = 0; i < aUsers.length; i++) {
        tr = table.insertRow(-1);
        var oUser = aUsers[i];
        for(var j=0; j<aReportRowKeys.length; j++) {
            tr.insertCell(-1).innerHTML = oUser[aReportRowKeys[j]];
        }
    }

    var divContainer = document.getElementById("reportTable");
    divContainer.innerHTML = "";
    divContainer.appendChild(table);

}


function getUTCDateTime(date) {
    if (date !== undefined) {
        var now = new Date(date);
    }
    else {
        var now = new Date();
    }
    return now.getUTCFullYear() + '-' + (now.getUTCMonth() + 1) + '-' + now.getUTCDate() + ' ' + now.getUTCHours() + ':' + now.getUTCMinutes() + ':' + now.getUTCSeconds()
}