import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import config from "../firebase";
require("date-utils");

firebase.initializeApp(config);

const urlParams = new URLSearchParams(window.location.search);

var payerId = urlParams.get("PayerID");
var paymentId = urlParams.get("paymentId");
var token = urlParams.get("token");

if (payerId == null || payerId == undefined) {
    window.location.replace("../pages/payment.html?paymentStatus=cancelled");
}
else {
    logPayment();

}

function logPayment() {
    const dateReceived = getUTCDateTime();
    //const sUser = firebase.auth().currentUser.uid;

    //const sUser = firebase.auth().currentUser.uid;
    const sUser = sessionStorage.sUser;
    firebase.database().ref("user/" + sUser + "/payment/" + dateReceived).set({
        paymentMethod: "PayPal",
        payerId: payerId,
        paymentId: paymentId,
        token: token
    }).then(() =>
        firebase.database().ref("user/" + sUser).update({
            userType: "paid"
        })
    ).then(() =>
        window.location.replace("../pages/dashboard.html?status=paymentAccepted")

    ).
        catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            console.log("Error:" + errorCode + ". " + errorMessage);
        })
}

function getUTCDateTime() {
    var now = new Date();
    return now.getUTCFullYear() + '-' + (now.getUTCMonth() + 1) + '-' + now.getUTCDate() + ' ' + now.getUTCHours() + ':' + now.getUTCMinutes() + ':' + now.getUTCSeconds()
}