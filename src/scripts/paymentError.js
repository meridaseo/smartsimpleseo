const urlParams = new URLSearchParams(window.location.search);


if (urlParams.get("paymentStatus")=="cancelled"){
    document.getElementById("paymentError").innerHTML="Looks like you cancelled the payment, try again?";
}
else if (urlParams.get("status")== "exceededUse"){
    document.getElementById("paymentError").innerHTML = "It looks like you've used our tool twice and will no longer be able to use it for free. But... You can get unlimited access for $9.99!";
}
else if(urlParams.get("status")=="signedUp"){
    document.getElementById("paymentError").innerHTML="Successfully signed up. Please complete payment for full access.";
}
else if (urlParams.get("status") == "pending") {
    document.getElementById("paymentError").innerHTML = "It looks like a payment is pending on your account.";
}
else{
    document.getElementById("paymentError").innerHTML="";
}
